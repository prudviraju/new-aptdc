# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_04_111437) do

  create_table "cart_items", force: :cascade do |t|
    t.integer "ticket_id"
    t.integer "no_of_items"
    t.boolean "is_updated"
    t.string "session_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "phone_no"
    t.string "email"
    t.string "address1"
    t.string "address2"
    t.string "town"
    t.string "state"
    t.string "postal_code"
    t.string "merchant_transaction_id"
    t.string "status"
    t.decimal "transaction_amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
