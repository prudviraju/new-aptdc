class CreateBranches < ActiveRecord::Migration[5.2]
  def change
    create_table :branches do |t|
      t.integer :city_id
      t.string :name
      t.string :branch_code
      t.integer :exit_id
      t.boolean :is_private,:defalt=> false;
      t.boolean :is_active,:defalt=> false;
      t.timestamps
    end
  end
end
