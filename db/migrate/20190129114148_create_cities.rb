class CreateCities < ActiveRecord::Migration[5.2]
  def change
    create_table :cities do |t|
      t.string :name
      t.boolean :active,:defalt=> false;
      t.integer :exit_id
      t.integer :tenantId

      t.timestamps
    end
  end
end
