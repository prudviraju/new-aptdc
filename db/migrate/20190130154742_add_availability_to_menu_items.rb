class AddAvailabilityToMenuItems < ActiveRecord::Migration[5.2]
  def change
    add_column :menu_items, :availability, :string
  end
end
