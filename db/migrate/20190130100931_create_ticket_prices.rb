class CreateTicketPrices < ActiveRecord::Migration[5.2]
  def change
    create_table :ticket_prices do |t|
      t.string :name
      t.decimal :price
      t.integer :city_id
      t.integer :branch_id
      t.integer :menu_id
      t.string :menu_name
      t.boolean :is_active,:defalt=> false;
      t.integer :exit_id

      t.timestamps
    end
  end
end
