class CreateMenuItems < ActiveRecord::Migration[5.2]
  def change
    create_table :menu_items do |t|
      t.string :name
      t.integer :image_id
      t.string :duration
      t.boolean :has_lay_out,:defalt=> false;
      t.boolean :is_active,:defalt=> false;
      t.string :is_private,:defalt=> false;
      t.integer :total_seats
      t.integer :branch_id
      t.integer :city_id

      t.timestamps
    end
  end
end
