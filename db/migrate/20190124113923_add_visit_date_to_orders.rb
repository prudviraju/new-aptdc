class AddVisitDateToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :visit_date, :string
  end
end
