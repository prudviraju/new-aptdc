class AddTransactionDetailsToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :transaction_details, :string
  end
end
