class CreateBookedCartItems < ActiveRecord::Migration[5.2]
  def change
    create_table :booked_cart_items do |t|
      t.string :city
      t.string :branch
      t.string :item
      t.string :ticket_name
      t.integer :quandity
      t.decimal :amount
      t.string :session_id

      t.timestamps
    end
  end
end
