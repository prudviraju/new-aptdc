class CreateOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.string :item_name
      t.integer :item_id
      t.integer :item_quandity
      t.decimal :cgst
      t.decimal :sgst
      t.decimal :price
      t.decimal :total_price
      t.string :city
      t.string :location
      t.string :item_menu_name

      t.timestamps
    end
  end
end
