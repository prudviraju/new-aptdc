class AddBookedToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :booked_to, :date
  end
end
