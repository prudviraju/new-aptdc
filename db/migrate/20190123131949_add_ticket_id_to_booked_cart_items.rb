class AddTicketIdToBookedCartItems < ActiveRecord::Migration[5.2]
  def change
    add_column :booked_cart_items, :ticket_id, :integer
    add_column :booked_cart_items, :tax, :decimal
  end
end
