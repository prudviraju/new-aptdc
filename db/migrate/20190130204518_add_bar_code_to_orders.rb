class AddBarCodeToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :barcode, :string
  end
end
