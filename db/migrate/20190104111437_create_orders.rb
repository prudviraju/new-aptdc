class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone_no
      t.string :email
      t.string :address1
      t.string :address2
      t.string :town
      t.string :state
      t.string :postal_code
      t.string :merchant_transaction_id
      t.string :status
      t.decimal :transaction_amount

      t.timestamps
    end
  end
end
