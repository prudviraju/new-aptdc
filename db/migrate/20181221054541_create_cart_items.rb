class CreateCartItems < ActiveRecord::Migration[5.2]
  def change
    create_table :cart_items do |t|
      t.integer :ticket_id
      t.integer :no_of_items
      t.boolean :is_updated
      t.string :session_id

      t.timestamps
    end
  end
end
