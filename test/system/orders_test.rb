require "application_system_test_case"

class OrdersTest < ApplicationSystemTestCase
  setup do
    @order = orders(:one)
  end

  test "visiting the index" do
    visit orders_url
    assert_selector "h1", text: "Orders"
  end

  test "creating a Order" do
    visit orders_url
    click_on "New Order"

    fill_in "Address1", with: @order.address1
    fill_in "Address2", with: @order.address2
    fill_in "Email", with: @order.email
    fill_in "First name", with: @order.first_name
    fill_in "Last name", with: @order.last_name
    fill_in "Merchant transaction", with: @order.merchant_transaction_id
    fill_in "Phone no", with: @order.phone_no
    fill_in "Postal code", with: @order.postal_code
    fill_in "State", with: @order.state
    fill_in "Status", with: @order.status
    fill_in "Town", with: @order.town
    fill_in "Transaction amount", with: @order.transaction_amount
    click_on "Create Order"

    assert_text "Order was successfully created"
    click_on "Back"
  end

  test "updating a Order" do
    visit orders_url
    click_on "Edit", match: :first

    fill_in "Address1", with: @order.address1
    fill_in "Address2", with: @order.address2
    fill_in "Email", with: @order.email
    fill_in "First name", with: @order.first_name
    fill_in "Last name", with: @order.last_name
    fill_in "Merchant transaction", with: @order.merchant_transaction_id
    fill_in "Phone no", with: @order.phone_no
    fill_in "Postal code", with: @order.postal_code
    fill_in "State", with: @order.state
    fill_in "Status", with: @order.status
    fill_in "Town", with: @order.town
    fill_in "Transaction amount", with: @order.transaction_amount
    click_on "Update Order"

    assert_text "Order was successfully updated"
    click_on "Back"
  end

  test "destroying a Order" do
    visit orders_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Order was successfully destroyed"
  end
end
