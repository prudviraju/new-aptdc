module CartItemsHelper

  class MenuItems
    attr_accessor :name, :taxInclusive, :branch_name,:city,:price,:id
    def initialize(name, taxInclusive, branch_name,city, price,id)
      @name = name
      @taxInclusive = taxInclusive
      @branch_name = branch_name
      @city = city
      @price = price
      @id = id
    end
  end
end
