module ApplicationHelper

  module SessionConstants
    SESSION_ID = :session_id
    SELECTED_MENU_ITEM = :selected_menu_item
    SELECTED_CITY = :selected_city
    SESSION_SERVICES = :session_services
    SESSION_BRANCHES = :session_branches
    SESSION_CART_ITEMS = :session_cart_items
  end

  MERCHANT_ID = "195866"
  SECRET_KEY_TO_GENERATE_HASH = "a9d16501d8e4d51b847fe4f181aa06b6426fc7c1e21617a78a63c5894b9a7597"
  MERCHANT_KEY_ID = "payment"

  def generate_qr(text)
    require 'barby'
    require 'barby/barcode'
    require 'barby/barcode/qr_code'
    require 'barby/outputter/png_outputter'
    require 'barby/barcode/code_128'
    require 'barby/outputter/ascii_outputter'
    # barcode = Barby::QrCode.new(text, level: :q, size: 5)
    barcode = Barby::Code128B.new(text)
    base64_output = Base64.encode64(barcode.to_png({ xdim: 5 }))
    "data:image/png;base64,#{base64_output}"
  end

  def get_index_text(value)
    "<div class=\"row\">
    <div class=\"col-sm-12\" style=\"padding-left: 0px;\">
    <div class=\"head_style\">" "#{value}</div></div></div>".html_safe
  end

  def cc_avenue_details(payment_details, return_url)
    values = {
        :order_id => payment_details.merchant_transaction_id,
        :amount => payment_details.transaction_amount,
        :currency => 'INR',
        :merchant_id => '195866',
        :redirect_url => return_url,
        :cancel_url => return_url,
        :language => 'EN',
        :billing_name => payment_details.first_name + (payment_details.last_name.present? ? ' ' + payment_details.last_name : ''),
        :billing_address => payment_details.address1,
        :billing_city => payment_details.town,
        :billing_zip => payment_details.postal_code,
        :billing_tel => payment_details.phone_no,
        :billing_email => payment_details.email,
        :billing_state => payment_details.state,
        :billing_country => 'India'
    }
    "http://bookings.aptdc.in/cc_avenue/ccavRequestHandler?" + values.to_query
  end

  def send_sms(mobile_number, original, message_type, order_id)

    puts ":########################"
    message = original.dup
    puts message
    puts ":########################"

    if mobile_number.to_s.strip.length == 10
      message.gsub!('%', "%25")
      message.gsub!('&', "%26")
      message.gsub!('+', "%2B")
      message.gsub!('#', "%23")
      message.gsub!('=', "%3D")
      message.gsub!(' ', "%20")
      url = "http://sms.txtdesk.com/WebServiceSMS.aspx?User=APTDC&passwd=APTDC123&mobilenumber=91"+mobile_number+"&sid=APTDCT&message="+message+"&fl=0&gwid=2"
      %x[echo "#{url}" >> sms-sending.log]
      %x[curl "#{url}" >> sms-sending.log]
      %x[echo "\n\n--------------------\n\n" >> sms-sending.log]
    end
  end

end
