json.extract! ticket_price, :id, :name, :price, :city_id, :branch_id, :menu_id, :menu_name, :is_active, :exit_id, :created_at, :updated_at
json.url ticket_price_url(ticket_price, format: :json)
