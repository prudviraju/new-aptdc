json.extract! cart_item, :id, :ticket_id, :no_of_items, :is_updated, :session_id, :created_at, :updated_at
json.url cart_item_url(cart_item, format: :json)
