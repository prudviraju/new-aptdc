json.extract! city, :id, :name, :active, :exit_id, :tenantId, :created_at, :updated_at
json.url city_url(city, format: :json)
