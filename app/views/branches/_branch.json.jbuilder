json.extract! branch, :id, :city_id, :name, :branch_code, :exit_id, :is_private, :is_active, :created_at, :updated_at
json.url branch_url(branch, format: :json)
