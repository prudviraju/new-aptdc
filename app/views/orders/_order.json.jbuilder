json.extract! order, :id, :first_name, :last_name, :phone_no, :email, :address1, :address2, :town, :state, :postal_code, :merchant_transaction_id, :status, :transaction_amount,:booked_to,:transaction_details,:visit_date,:is_accepted, :created_at, :updated_at,:barcode
json.url order_url(order, format: :json)
