json.extract! menu_item, :id, :name, :image_id, :duration, :has_lay_out, :is_active, :is_private, :total_seats, :branch_id, :city_id, :created_at, :updated_at
json.url menu_item_url(menu_item, format: :json)
