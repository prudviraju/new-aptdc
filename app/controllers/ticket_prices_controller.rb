class TicketPricesController < ApplicationController
  before_action :set_ticket_price, only: [:show, :edit, :update, :destroy]

  # GET /ticket_prices
  # GET /ticket_prices.json
  def index
    @ticket_prices = TicketPrice.all
  end

  # GET /ticket_prices/1
  # GET /ticket_prices/1.json
  def show
  end

  # GET /ticket_prices/new
  def new
    @ticket_price = TicketPrice.new
  end

  # GET /ticket_prices/1/edit
  def edit
  end

  # POST /ticket_prices
  # POST /ticket_prices.json
  def create
    @ticket_price = TicketPrice.new(ticket_price_params)

    respond_to do |format|
      if @ticket_price.save
        format.html { redirect_to "/ticket_prices", notice: 'Ticket price was successfully created.' }
        format.json { render :show, status: :created, location: @ticket_price }
      else
        format.html { render :new }
        format.json { render json: @ticket_price.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ticket_prices/1
  # PATCH/PUT /ticket_prices/1.json
  def update
    respond_to do |format|
      if @ticket_price.update(ticket_price_params)
        format.html { redirect_to "/ticket_prices", notice: 'Ticket price was successfully updated.' }
        format.json { render :show, status: :ok, location: @ticket_price }
      else
        format.html { render :edit }
        format.json { render json: @ticket_price.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ticket_prices/1
  # DELETE /ticket_prices/1.json
  def destroy
    @ticket_price.destroy
    respond_to do |format|
      format.html { redirect_to ticket_prices_url, notice: 'Ticket price was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket_price
      @ticket_price = TicketPrice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_price_params
      params.require(:ticket_price).permit(:name, :price, :city_id, :branch_id, :menu_id, :menu_name, :is_active, :exit_id)
    end
end
