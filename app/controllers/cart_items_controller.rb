class CartItemsController < ApplicationController
  require 'httparty'
  require 'uri'
  require 'net/http'
  include ApplicationHelper
  include CartItemsHelper
  include CcAvenueHelper
  skip_before_action :verify_authenticity_token
  before_action :set_cart_item, only: [:show, :edit, :update, :destroy]

  $selectedCity = ''
  $slectedCity_branchList =[]
  $slectedCity_branch = ''
  $vijayavada_branches = ["Bermpark","Bhavani Island Tickets"]
  $menuItems = []
  # GET /cart_items
  # GET /cart_items.json
  def index
    @cart_items = CartItem.all
  end

  # GET /cart_items/1
  # GET /cart_items/1.json
  def show
  end

  # GET /cart_items/new
  def new
    @cart_item = CartItem.new
  end

  # GET /cart_items/1/edit
  def edit
  end

  # POST /cart_items
  # POST /cart_items.json
  def create
    @cart_item = CartItem.new(cart_item_params)

    respond_to do |format|
      if @cart_item.save
        format.html { redirect_to @cart_item, notice: 'Cart item was successfully created.' }
        format.json { render :show, status: :created, location: @cart_item }
      else
        format.html { render :new }
        format.json { render json: @cart_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cart_items/1
  # PATCH/PUT /cart_items/1.json
  def update
    respond_to do |format|
      if @cart_item.update(cart_item_params)
        format.html { redirect_to @cart_item, notice: 'Cart item was successfully updated.' }
        format.json { render :show, status: :ok, location: @cart_item }
      else
        format.html { render :edit }
        format.json { render json: @cart_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cart_items/1
  # DELETE /cart_items/1.json
  def destroy
    @cart_item.destroy
    respond_to do |format|
      format.html { redirect_to cart_items_url, notice: 'Cart item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def homePage
    @cities = City.where("active=?",true)
    $selectedCity = @cities[0].id
    $slectedCity_branchList = Branch.where("city_id=? and is_active=?",$selectedCity,true)
    $slectedCity_branch = $slectedCity_branchList[0].id
    $menuItems = []
    $menuItems = MenuItem.where("branch_id=? and is_active=?",$slectedCity_branch,true)
    render :layout => 'application1'
  end



  def cityBranchMenu
    $slectedCity_branch = params[:branch_name]
    $menuItems = []
    $menuItems = MenuItem.where("branch_id=? and is_active=?",$slectedCity_branch,true)
    render :layout => 'application1'
  end



  def place_order
    @order = Order.new
    @order.first_name = params[:order][:first_name]
    @order.last_name = params[:order][:last_name]
    @order.phone_no = params[:order][:phone_no]
    @order.email = params[:order][:email]
    @order.visit_date = params[:order][:visit_date]
    @order.status = 'Created'
    random_id = UUIDTools::UUID.random_create.to_s.delete '-'
    random_id[0..2] = ''
    @order.merchant_transaction_id = random_id
    @order.is_accepted = true
    @order.save!
    totalPrice = 0.00

    @cartItems = BookedCartItem.where('session_id =?', session[SessionConstants::SESSION_ID])
    @cartItems.each do |item|
    @order_item = OrderItem.new
    @order_item.order_id = @order.id
    @order_item.item_name = item.ticket_name
    @order_item.item_id = 0
    @order_item.item_quandity = item.quandity
    @order_item.price = item.amount
    @order_item.total_price = item.amount * item.quandity
    @order_item.city = item.city
    @order_item.location = item.branch
    @order_item.item_menu_name = item.item
    @order_item.save
    totalPrice = totalPrice +  @order_item.total_price
    end
    @order.transaction_amount = totalPrice
    @order.ticket_no = "APTDC#{Date.today}#{@order.id}"
    @order.barcode = generate_qr("APTDC#{Date.today}#{@order.id}")
    @order.save
    if @order.present?
      @all_order_items = OrderItem.where("order_id=?", @order.id)
    end
    reset_session
    if @order.present?
      payment_url = cc_avenue_details(@order, 'http://bookings.aptdc.in/cart_items/pay_zippy_response')
      redirect_to payment_url
    else
      redirect_to "/"
    end
  end


  def pay_zippy_response
    workingKey="3700EF613748D45F99A6183F16285948"  #Put in the 32 Bit Working Key provided by CCAVENUES.
    encResponse=params[:encResp]
    decResp=decrypt(encResponse,workingKey);
    decResp = decResp.split("&")
    @ccavenueParams = {}
    decResp.each do |key|
      @ccavenueParams[key.from(0).to(key.index("=")-1)] = key.from(key.index("=")+1).to(-1)
    end
    @order = Order.find_by_merchant_transaction_id(@ccavenueParams["order_id"])
    if @order.present?
      @order.status = @ccavenueParams["order_status"]
      @order.transaction_details = @ccavenueParams
      @order.save
    end
    if (@order.present? && @order.status == 'Success') || (@order.present? && @order.status == 'Completed')
      sms = "Dear #{@order.first_name}Your order was booked at APTDC with OrderID #{@order.id},TrnID #{@order.merchant_transaction_id},Fare #{@order.transaction_amount}."
      send_sms(@order.phone_no, sms, 'order booked', @order.id)
      @order_items = OrderItem.where('order_id =?', @order.id)
      UserMailer.orderbooked(@order, @order_items).deliver


      # # CRM  INTIGRATION-------------------------------------------------------------->
      # @auth_token = ''
      # # authToken CALL
      # response =HTTParty.post("https://login5.responsys.net/rest/api/v1.3/auth/token?user_name=aptdc_di&password=Welcome@123$&auth_type=password")
      # @auth_token = response["authToken"]
      # # authToken CALL End
      # url1 = URI("https://api5-023.responsys.net/rest/api/v1.3/lists/CONTACTS_LIST/members")
      # http = Net::HTTP.new(url1.host, url1.port)
      # http.use_ssl = true
      # request1 = Net::HTTP::Post.new(url1)
      # request1["Authorization"] = "#{@auth_token}"
      # request1["Content-Type"] = 'application/json'
      # request1["cache-control"] = 'no-cache'
      # request1["Postman-Token"] = '34265ca8-66a9-4cd9-9b1e-160ba6d18130'
      # request1.body = "{ \r\n\"recordData\" : {\r\n\"fieldNames\" : [ \"MOBILE_NUMBER_\",\"MOBILE_COUNTRY_\", \"EMAIL_ADDRESS_\", \"FIRST_NAME\",\"LAST_NAME\"],\r\n\"records\" : [\r\n[\"+91#{@order.phone_no}\",\"IN\",\"#{@order.email}\", \"#{@order.first_name}\",\"#{@order.last_name}\"]\r\n],\r\n\"mapTemplateName\" : null\r\n},\r\n\"mergeRule\" : {\r\n\"htmlValue\" : \"H\",\r\n\"optinValue\" : \"I\",\r\n\"textValue\" : \"T\",\r\n\"insertOnNoMatch\" : true,\r\n\"updateOnMatch\" : \"REPLACE_ALL\",\r\n\"matchColumnName1\" : \"MOBILE_NUMBER_\",\r\n\"matchColumnName2\" : null,\r\n\"matchOperator\" : \"NONE\",\r\n\"optoutValue\" : \"O\",\r\n\"rejectRecordIfChannelEmpty\" : null,\r\n\"defaultPermissionStatus\" : \"OPTIN\"\r\n}\r\n}"
      # response1 = http.request(request1)
      # url2 = URI("https://ejaa.fa.em2.oraclecloud.com/crmRestApi/resources/11.13.18.05/contacts")
      # http = Net::HTTP.new(url2.host, url2.port)
      # http.use_ssl = true
      # request2 = Net::HTTP::Post.new(url2)
      # request2["Authorization"] = "#{@auth_token}"
      # request2["Content-Type"] = 'application/json'
      # request2["Authorization"] = 'Basic cmFtYW5hQGFwdGRjLmluOkFwdGRjMjAxOCE='
      # request2["cache-control"] = 'no-cache'
      # request2["Postman-Token"] = 'cb75b9dc-9bb6-4289-a856-bb89f295953a'
      # request2.body = "{\r\n    \"FirstName\": \"#{@order.first_name}\",\r\n    \"LastName\": \"#{@order.last_name}\",\r\n    \"EmailAddress\": \"#{@order.email}\",\r\n    \"MobileNumber\":\"+91#{@order.phone_no}\",\r\n    \"Address\": [\r\n        {\r\n            \"Address1\": \"Malaysian Township\",\r\n            \"City\": \"Hyderabad\",\r\n            \"Country\": \"IN\",\r\n            \"State\": \"TS\"\r\n        }\r\n    ]\r\n}"
      # response2 = http.request(request2)
      # url3 = URI("https://api5-023.responsys.net/rest/api/v1.3/lists/CONTACTS_LIST/listExtensions/CRM_HOTEL_CHECKINS/members")
      # http = Net::HTTP.new(url3.host, url3.port)
      # http.use_ssl = true
      # request3 = Net::HTTP::Post.new(url3)
      # request3["Authorization"] = "#{@auth_token}"
      # request3["Content-Type"] = 'application/json'
      # request3["cache-control"] = 'no-cache'
      # request3["Postman-Token"] = '4bb83278-77f8-46d9-8d81-cf5d1c33cca3'
      # request3.body = "{\n\"recordData\" : {\n\"fieldNames\" : [\"MOBILE_NUMBER_\", \"RESERVATION_NO\", \"UNIT_NAME\", \"GUEST_EMAIL_ID\", \"RESERVATION_DATE\", \"EXP_CHECKIN_DATE\", \"EXP_CHECKOUT_DATE\", \"RESERVATION_TYPE\", \"SERVICE_TYPE\"],\n\"records\" : [\n[\"+91#{@order.phone_no}\", \"#{@order.ticket_no}\", \"#{@order_items[0].location} - #{@order_items[0].item_menu_name}\", \"#{@order.email}\", \"#{@order.visit_date}\", \"#{@order.visit_date}\", \"#{@order.visit_date}\", \"ONLINE\", \"BOATING\"]\n],\n\"mapTemplateName\" : null\n},\n\"insertOnNoMatch\" : true,\n\"updateOnMatch\" : \"REPLACE_ALL\",\n\"matchColumnName1\" : \"MOBILE_NUMBER\",\n\"matchColumnName2\" : null\n}"
      # response3 = http.request(request3)
      # url4 = URI("https://api5-023.responsys.net/rest/api/v1.3/folders/%21MasterData/suppData/AP_Hotel_CRM/members")
      # http = Net::HTTP.new(url4.host, url4.port)
      # http.use_ssl = true
      # request4 = Net::HTTP::Post.new(url4)
      # request4["Authorization"] = 'E5yh2U3Gs51gUHJG1l5F4VK7jpNWE1c61P3nXzYkWBLIvyJsUtoDYw'
      # request4["Content-Type"] = 'application/json'
      # request4["cache-control"] = 'no-cache'
      # request4["Postman-Token"] = '9a0f977e-b309-4129-a4f6-a028395c3402'
      # request4.body = "{\r\n  \"recordData\" : {\r\n    \"fieldNames\" : [\"EMAIL_ADDRESS_\", \"MOBILE_NUMBER_\", \"RESERVATION_TYPE\", \"UNIT_NAME\", \"RESERVATION_NO\", \"SERVICE_TYPE\", \"RESERVATION_DATE\", \"EXP_CHECKIN_DATE\",\"EXP_CHECKOUT_DATE\"],\r\n    \"records\" : [\r\n                [\"#{@order.email}\", \"+91#{@order.phone_no}\", \"Online\", \"#{@order_items[0].location}\", \"#{@order.ticket_no}\",\"BOATING\", \"#{@order.visit_date}\",\"#{@order.visit_date}\",\"#{@order.visit_date}\"]\r\n                ],\r\n    \"mapTemplateName\" : null\r\n  },\r\n   \"insertOnNoMatch\" : true,\r\n   \"updateOnMatch\" : \"REPLACE_ALL\"\r\n}\r\n  "
      # response4 = http.request(request4)
      # CRM  INTIGRATION END-------------------------------------------------------------->
      @order.is_check_crm = true
      @order.save
      redirect_to "/cart_items/orderRecipt?order_id=#{@order.id}"
    else
      redirect_to "/cart_items/orderRecipt?transaction_details"
    end

  end


  def orderRecipt
    if params[:order_id].present?
      @order = Order.find_by_id(params[:order_id])
      @orderItems = OrderItem.where('order_id =?', @order.id)
    end
    respond_to do |format|
      if params[:print].present?
        format.html {render :layout => false}
      else
        format.html{render :layout => 'application1'}

      end

      format.pdf do
        render pdf: "APTDC_#{@order.id}",
               :margin => {:top                => 5,
                           :bottom             => 5,
                           :left               => 0,
                           :right              => 0},
               :orientation      => 'portrait'
      end
    end
  end




  def contactUs

    render :layout => 'application1'
  end
  def aboutUs

    render :layout => 'application1'
  end

  def division_based_branches
    $selectedCity =  params["division"]
    $slectedCity_branchList = Branch.where("city_id=? and is_active=?",$selectedCity,true)
    $slectedCity_branch = $slectedCity_branchList[0].id
    $menuItems = []
    $menuItems = MenuItem.where("branch_id=? and is_active=?",$slectedCity_branch,true)
    render :layout => 'application1'
  end


  def branch_menu
    if params["code"].present?
      @branch = params[:branch]
      @branchServiceType = ''
      $menuItems = []
      @branchParentMenu = HTTParty.get("http://aptdc.eposfx.com/epos/interface/getParentMenuCategories?tenant=VIJAYAWADA&branchCode=#{params["code"]}")
      @branchMenuItemPrice = HTTParty.get("http://aptdc.eposfx.com/epos/interface/menusWithOptions?tenant=VIJAYAWADA&branchCode=#{params["code"]}")
      @branchProductMenu = HTTParty.get("http://aptdc.eposfx.com/epos/interface/products?tenant=VIJAYAWADA&branchCode=#{params["code"]}")

      @branchParentMenu.each do |menu|
        @allmenuPrices = @branchMenuItemPrice.select {|favor| favor["category"] == menu['name']}
        @serviceType = @branchProductMenu.select {|favor| favor["category"] == menu['name']}
        $menuItems << MenuItems.new(menu["name"], menu["taxInclusive"], @branch,$selectedCity, @allmenuPrices,'')
      end
      @branchServiceType = @serviceType[0]['brand']
    else
      redirect_to "/"
    end
  end

  def display_cart
    @selectedItem = MenuItem.find_by_id(params[:menuId]) if params[:menuId].present?
    if @selectedItem.present?
      @order = Order.new
      reset_session
      render :layout => 'application1'
    else
      redirect_to "/"
    end
  end


  def change_cart_items
    session[SessionConstants::SESSION_ID] = UUIDTools::UUID.random_create.to_s.delete '-' if session[SessionConstants::SESSION_ID].blank?
    if params[:ticketType].present? & params[:no_of_items].present?
      @cartItem = BookedCartItem.find_by_ticket_name_and_session_id(params[:ticketType], session[SessionConstants::SESSION_ID])
      @selectedItem = MenuItem.find_by_id(params["item"])
      if @cartItem.blank? && !params[:no_of_items].equal?(0)
        @cartItem = BookedCartItem.new
        @cartItem.city = City.find_by_id(@selectedItem.city_id).try(:name)
        @cartItem.branch = Branch.find_by_id(@selectedItem.branch_id).try(:name)
        @cartItem.item = @selectedItem.name
        @cartItem.ticket_name = params[:ticketType]
        @cartItem.quandity = params[:no_of_items]
        @cartItem.amount = params[:price]
        @cartItem.session_id = session[SessionConstants::SESSION_ID]
        @cartItem.ticket_id = -999
        @cartItem.save
      else
        if params[:no_of_items].to_i == 0
          @cartItem.destroy
        else
          @cartItem.quandity = params[:no_of_items]
          @cartItem.amount = params[:price]
          @cartItem.save
        end
      end
    else
      if params[:cart_id].present?
        @cartItem = BookedCartItem.find_by_id(params[:cart_id])
        @cartItem.destroy if @cartItem.present?
      end
    end
  end


  def test_crm
    @order = Order.all.last
    @order_items = OrderItem.where("order_id = ?" ,@order.id)
    @auth_token = ''
    # authToken CALL
    response =HTTParty.post("https://login5.responsys.net/rest/api/v1.3/auth/token?user_name=aptdc_di&password=Welcome@123$&auth_type=password")
    @auth_token = response["authToken"]
    # authToken CALL End
    url1 = URI("https://api5-023.responsys.net/rest/api/v1.3/lists/CONTACTS_LIST/members")
    http = Net::HTTP.new(url1.host, url1.port)
    http.use_ssl = true
    request1 = Net::HTTP::Post.new(url1)
    request1["Authorization"] = "#{@auth_token}"
    request1["Content-Type"] = 'application/json'
    request1["cache-control"] = 'no-cache'
    request1["Postman-Token"] = '34265ca8-66a9-4cd9-9b1e-160ba6d18130'
    request1.body = "{ \r\n\"recordData\" : {\r\n\"fieldNames\" : [ \"MOBILE_NUMBER_\",\"MOBILE_COUNTRY_\", \"EMAIL_ADDRESS_\", \"FIRST_NAME\",\"LAST_NAME\"],\r\n\"records\" : [\r\n[\"+91#{@order.phone_no}\",\"IN\",\"#{@order.email}\", \"#{@order.first_name}\",\"#{@order.last_name}\"]\r\n],\r\n\"mapTemplateName\" : null\r\n},\r\n\"mergeRule\" : {\r\n\"htmlValue\" : \"H\",\r\n\"optinValue\" : \"I\",\r\n\"textValue\" : \"T\",\r\n\"insertOnNoMatch\" : true,\r\n\"updateOnMatch\" : \"REPLACE_ALL\",\r\n\"matchColumnName1\" : \"MOBILE_NUMBER_\",\r\n\"matchColumnName2\" : null,\r\n\"matchOperator\" : \"NONE\",\r\n\"optoutValue\" : \"O\",\r\n\"rejectRecordIfChannelEmpty\" : null,\r\n\"defaultPermissionStatus\" : \"OPTIN\"\r\n}\r\n}"
    response1 = http.request(request1)
    url2 = URI("https://ejaa.fa.em2.oraclecloud.com/crmRestApi/resources/11.13.18.05/contacts")
    http = Net::HTTP.new(url2.host, url2.port)
    http.use_ssl = true
    request2 = Net::HTTP::Post.new(url2)
    request2["Authorization"] = "#{@auth_token}"
    request2["Content-Type"] = 'application/json'
    request2["Authorization"] = 'Basic cmFtYW5hQGFwdGRjLmluOkFwdGRjMjAxOCE='
    request2["cache-control"] = 'no-cache'
    request2["Postman-Token"] = 'cb75b9dc-9bb6-4289-a856-bb89f295953a'
    request2.body = "{\r\n    \"FirstName\": \"#{@order.first_name}\",\r\n    \"LastName\": \"#{@order.last_name}\",\r\n    \"EmailAddress\": \"#{@order.email}\",\r\n    \"MobileNumber\":\"+91#{@order.phone_no}\",\r\n    \"Address\": [\r\n        {\r\n            \"Address1\": \"Malaysian Township\",\r\n            \"City\": \"Hyderabad\",\r\n            \"Country\": \"IN\",\r\n            \"State\": \"TS\"\r\n        }\r\n    ]\r\n}"
    response2 = http.request(request2)
    url3 = URI("https://api5-023.responsys.net/rest/api/v1.3/lists/CONTACTS_LIST/listExtensions/CRM_HOTEL_CHECKINS/members")
    http = Net::HTTP.new(url3.host, url3.port)
    http.use_ssl = true
    request3 = Net::HTTP::Post.new(url3)
    request3["Authorization"] = "#{@auth_token}"
    request3["Content-Type"] = 'application/json'
    request3["cache-control"] = 'no-cache'
    request3["Postman-Token"] = '4bb83278-77f8-46d9-8d81-cf5d1c33cca3'
    request3.body = "{\n\"recordData\" : {\n\"fieldNames\" : [\"MOBILE_NUMBER_\", \"RESERVATION_NO\", \"UNIT_NAME\", \"GUEST_EMAIL_ID\", \"RESERVATION_DATE\", \"EXP_CHECKIN_DATE\", \"EXP_CHECKOUT_DATE\", \"RESERVATION_TYPE\", \"SERVICE_TYPE\"],\n\"records\" : [\n[\"+91#{@order.phone_no}\", \"#{@order.ticket_no}\", \"#{@order_items[0].location} - #{@order_items[0].item_menu_name}\", \"#{@order.email}\", \"#{@order.visit_date}\", \"#{@order.visit_date}\", \"#{@order.visit_date}\", \"ONLINE\", \"BOATING\"]\n],\n\"mapTemplateName\" : null\n},\n\"insertOnNoMatch\" : true,\n\"updateOnMatch\" : \"REPLACE_ALL\",\n\"matchColumnName1\" : \"MOBILE_NUMBER\",\n\"matchColumnName2\" : null\n}"
    response3 = http.request(request3)
    url4 = URI("https://api5-023.responsys.net/rest/api/v1.3/folders/%21MasterData/suppData/AP_Hotel_CRM/members")
    http = Net::HTTP.new(url4.host, url4.port)
    http.use_ssl = true
    request4 = Net::HTTP::Post.new(url4)
    request4["Authorization"] = "#{@auth_token}"
    request4["Content-Type"] = 'application/json'
    request4["cache-control"] = 'no-cache'
    request4["Postman-Token"] = '9a0f977e-b309-4129-a4f6-a028395c3402'
    request4.body = "{\r\n  \"recordData\" : {\r\n    \"fieldNames\" : [\"EMAIL_ADDRESS_\", \"MOBILE_NUMBER_\", \"RESERVATION_TYPE\", \"UNIT_NAME\", \"RESERVATION_NO\", \"SERVICE_TYPE\", \"RESERVATION_DATE\", \"EXP_CHECKIN_DATE\",\"EXP_CHECKOUT_DATE\"],\r\n    \"records\" : [\r\n                [\"#{@order.email}\", \"+91#{@order.phone_no}\", \"Online\", \"#{@order_items[0].location}\", \"#{@order.ticket_no}\",\"BOATING\", \"#{@order.visit_date}\",\"#{@order.visit_date}\",\"#{@order.visit_date}\"]\r\n                ],\r\n    \"mapTemplateName\" : null\r\n  },\r\n   \"insertOnNoMatch\" : true,\r\n   \"updateOnMatch\" : \"REPLACE_ALL\"\r\n}\r\n  "
    response4 = http.request(request4)
    respond_to do |format|
      format.json { render :json => {:response =>response["authToken"],:response1=> response1.read_body,:response2=> response2.read_body,:response3=>response3.read_body,:response4 => response4.read_body} }
    end

  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart_item
      @cart_item = CartItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cart_item_params
      params.require(:cart_item).permit(:ticket_id, :no_of_items, :is_updated, :session_id)
    end
end
