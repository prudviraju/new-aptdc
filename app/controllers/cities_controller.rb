class CitiesController < ApplicationController
  before_action :set_city, only: [:show, :edit, :update, :destroy]

  # GET /cities
  # GET /cities.json
  def index
    @cities = City.all
  end

  # GET /cities/1
  # GET /cities/1.json
  def show
  end

  # GET /cities/new
  def new
    @city = City.new
  end

  # GET /cities/1/edit
  def edit
  end

  # POST /cities
  # POST /cities.json
  def create
    @city = City.new(city_params)

    respond_to do |format|
      if @city.save
        format.html { redirect_to "/cities", notice: 'City was successfully created.' }
        format.json { render :show, status: :created, location: @city }
      else
        format.html { render :new }
        format.json { render json: @city.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cities/1
  # PATCH/PUT /cities/1.json
  def update
    respond_to do |format|
      if @city.update(city_params)
        format.html { redirect_to "/cities", notice: 'City was successfully updated.' }
        format.json { render :show, status: :ok, location: @city }
      else
        format.html { render :edit }
        format.json { render json: @city.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cities/1
  # DELETE /cities/1.json
  def destroy
    @city.destroy
    respond_to do |format|
      format.html { redirect_to cities_url, notice: 'City was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def update_data_base
    @oldcities = HTTParty.get("http://aptdc.eposfx.com/epos/interface/territories?tenant=VIJAYAWADA")
    @oldcities.each do |response|
      @city = City.find_by_exit_id(response["id"])
      @city = City.new if @city.blank?
      @city.name = response["name"]
      @city.tenantId = response["tenantId"]
      @city.active = response["active"]
      @city.exit_id = response["id"]
      @city.save
      if @city.present?
      @oldcitybranches = HTTParty.get("http://aptdc.eposfx.com/epos/interface/branchTerritories?tenant=VIJAYAWADA&territory=#{@city.name}")
      @oldcitybranches.each do |branch_response|
        @branch = Branch.find_by_exit_id(branch_response["id"])
        @branch = Branch.new if @branch.blank?
        @branch.city_id = @city.id
        @branch.name = branch_response['name']
        @branch.branch_code = branch_response['code']
        @branch.exit_id = branch_response['id']
        @branch.is_active = true
        @branch.is_private = false
        @branch.save
      end
      end
    end
    redirect_to "/cities"
  end


  def update_data_base1
    @branches = Branch.all
    @branches.each do |branch|
      @old_branch_menu = HTTParty.get("http://aptdc.eposfx.com/epos/interface/branchWiseTerritories?tenant=VIJAYAWADA&territory=#{City.find_by_id(branch.city_id).try(:name)}")
      @old_branch_menu["branches"].each do |menu_response|
        if menu_response['code'] == branch.branch_code
          menu_response["branchsList"].each do |menu|
            @menu_item = MenuItem.find_by_name_and_branch_id(menu["name"], branch.id)
            @menu_item = MenuItem.new if @menu_item.blank?
            @menu_item.name = menu["name"]
            @menu_item.is_active = true
            @menu_item.is_private = false
            @menu_item.branch_id = branch.id
            @menu_item.city_id = branch.city_id
            @menu_item.save
            menu["price"].each do |price_res|
              @ticket_price = TicketPrice.find_by_exit_id_and_menu_id(price_res["id"],@menu_item.id)
              @ticket_price = TicketPrice.new if @ticket_price.blank?
              @ticket_price.name = price_res["name"]
              @ticket_price.price = price_res["price"]
              @ticket_price.city_id = branch.city_id
              @ticket_price.branch_id = branch.id
              @ticket_price.menu_id = @menu_item.id
              # @ticket_price.menu_name = @menu_item.name
              @ticket_price.is_active = true
              @ticket_price.exit_id = price_res["id"]
              @ticket_price.save
            end

          end
        end
      end
    end
    redirect_to "/cities"
  end



  def all_cities
    @cities = City.where("active=?",true)
    respond_to do |format|
      format.json { render json: @cities }
    end
  end


  def locations_based_on_city
    @city = City.find_by_id(params[:city_id])
    @branches = Branch.where("city_id=? and is_active=?", params[:city_id], true)

    respond_to do |format|
      format.json { render json: @branches }
    end
  end



  def menu_based_on_location
    @city = City.find_by_id(params[:city_id])
    @branches = Branch.find_by_id(params[:branch_id])
    @menu_items = MenuItem.where("city_id=? and is_active=? and branch_id=?", params[:city_id], true,params[:branch_id])
    respond_to do |format|
      format.json { render json: @menu_items }
    end
  end


  def tickets_based_on_menu
    @menu_item = MenuItem.where("city_id=? and is_active=? and branch_id=? and id=?", params[:city_id], true,params[:branch_id],params[:menu_id] ).first
    @ticket_prices = TicketPrice.where("menu_id=?",@menu_item.id)
    respond_to do |format|
      format.json { render json: @ticket_prices }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_city
      @city = City.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def city_params
      params.require(:city).permit(:name, :active, :exit_id, :tenantId)
    end
end
