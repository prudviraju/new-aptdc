class UserMailer < ActionMailer::Base
  default from: "\"APTDC\" <aptdcupdates@gmail.com>"
  default_url_options[:host] = "bookings.aptdc.in"

  def orderbooked(order, orderItems)
    @order = order
    @orderItems = orderItems
    mail(:to => @order.email.to_s, :subject => 'APTDC Tickets Booked', :Bcc => "saisrujan0929@gmail.com") do |format|
      format.html # renders send_report.text.erb for body of email
      format.pdf do
        attachments["APTDC#{Date.today}#{@order.id}-booked.pdf"] = WickedPdf.new.pdf_from_string(
            render_to_string(:pdf => "APTDC#{Date.today}#{@order.id}-booked", :template => '/cart_items/orderRecipt.pdf')
        )
      end
    end
  end
end


