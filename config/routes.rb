Rails.application.routes.draw do
  resolve("ActiveStorage::Variant") {|variant, options| main_app.route_for(:rails_variant, variant, options)}


  resources :ticket_prices
  resources :menu_items
  resources :branches
  resources :orders
  resources :cities do
    get :update_data_base, :on => :collection
    get :update_data_base1, :on => :collection
    get :all_cities, :on => :collection
    get :locations_based_on_city, :on => :collection
    get :menu_based_on_location, :on => :collection
    get :tickets_based_on_menu, :on => :collection
  end

  resources :cart_items do
    get :homePage, :on => :collection
    get :division_based_branches, :on => :collection
    get :branch_menu, :on => :collection
    get :display_cart, :on => :collection
    get :change_cart_items, :on => :collection
    get :contactUs, :on => :collection
    get :aboutUs, :on => :collection
    post :place_order, :on => :collection
    post :pay_zippy_response, :on => :collection
    get :orderRecipt, :on => :collection
    get :cityBranchMenu, :on => :collection
    get :test_crm, :on => :collection
  end

  get "cc_avenue/dataFrom"

  get "cc_avenue/ccavRequestHandler"

  post "cc_avenue/ccavResponseHandler"

  root 'cart_items#homePage'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
